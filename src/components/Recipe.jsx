import React from "react";
import style from "./Recipe.module.css";

const Recipe = ({ title, calories, image, ingredients }) => {
  return (
    <div className={style.recipe}>
      <h1>{title}</h1>   <img className = {style.image} src={image} alt="" />
      {/* <h3>Calories : {calories}</h3>  */}
      <ul>
        {ingredients.map(ingredient => (
          <li>{ingredient.text}</li>
        ))}
      </ul>
           
          </div>
  );
};
export default Recipe;
