import React, { useEffect, useState } from "react";
import "./App.css";
import Recipe from "./components/Recipe";

const App = () => {
  const APP_ID = "62dfcbc0";
  const APP_KEY = "225dbca08a164aa8fbad0e4ac2cbe5e2";
    

  const [recipes, setRecipes] = useState([]);
  const [search, setSearch] = useState("");
  const [query, setQuery] = useState("chicken");

  useEffect(() => {
    getRecipes();
  }, [query]);

  const getRecipes = async () => {
    const response = await fetch(
    `https://api.edamam.com/search?q=${query}&app_id=${APP_ID}&app_key=${APP_KEY}`
); //pour le fetch mettre les guillement obtenu par AltGr+7
    const data = await response.json();
    setRecipes(data.hits);
    console.log(data.hits);
  };

  const updateSearch = e => {
    setSearch(e.target.value);
  };

  const getSearch = e => {
    e.preventDefault(); //pour que la page ne fetch pas a chaque fois que l'on tape une lettre
    setQuery(search);
    setSearch(""); //Pour vider le champs de recherche a chaque fois
  }

  return (
    <div className="App">
      <form onSubmit={getSearch} className="search-form">
      <input 
        className = "search-bar" 
        type = "text" 
        value = {search}
        onChange = {updateSearch}/>
        <button className="search-button" type="submit">
          Search
        </button>
      </form>
      <div className="recipes">
      {recipes.map(recipe => (
        <Recipe
          key={recipe.recipe.label}
          title={recipe.recipe.label}
          calories={recipe.recipe.calories}
          image={recipe.recipe.image}
          ingredients={recipe.recipe.ingredients}
        />
      ))}
      </div>
    </div> 
  );
};

export default App;
